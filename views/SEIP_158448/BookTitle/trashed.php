<?php
require_once("../../../vendor/autoload.php");
require_once("../../../src/BITM/SEIP_158448/BookTiltle/BookTitle.php");

$objBookTitle = new \App\BookTitle\BookTitle();
$allData = $objBookTitle->tarshed();
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title- active list</title>

    <style>
        td{
            border:1px;
        }

    </style>
</head>
<body>

    <h1>Book Title- Trashed list</h1>

    <table border="1px" cellspacing="0" cellpadding="0">
        <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Book Name</th>
        <th>Author Name</th>
        </tr>
        <?php
            $serial= 1;
            foreach($allData as $oneData){

                if($serial%2) $bgColor = "#cccccc";
                else $bgColor ="#ffffff";
                echo"
                    <tr style='backgroud-color: $bgColor'>
                        <td>$serial</td>
                        <td>$oneData->id</td>
                        <td>$oneData->book_name</td>
                        <td>$oneData->author_name</td>
                        <td></td>
                    </tr>
                ";

                $serial++;
            }
        ?>

    </table>

</body>
</html>