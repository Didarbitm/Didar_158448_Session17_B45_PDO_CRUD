<?php
require_once("../../../vendor/autoload.php");
require_once("../../../src/BITM/SEIP_158448/BookTiltle/BookTitle.php");
require_once("../../../src/BITM/SEIP_158448/Massage/Message.php");


$objBookTitle = new \App\BookTitle\BookTitle();
$allData = $objBookTitle->index();


use App\Message\Message;

if(!isset($_SESSION)) {
    session_start();
}

$msg = Message::getMessage();

echo "<div id = 'message'>$msg</div>";
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title- active list</title>

    <style>


    </style>
</head>
<body>

    <h1>Book Title- active list</h1>

    <table border="1px" cellspacing="0" cellpadding="0">
        <tr>
            <th>Serial Number</th>
            <th>ID</th>
            <th>Book Name</th>
            <th>Author Name</th>
            <th>Action</th>
        </tr>
        <?php
            $serial= 1;
            foreach($allData as $oneData){

                if($serial%2) $bgColor = "#cccccc";
                else $bgColor ="#ffffff";
                echo"
                    <tr style='backgroud-color: $bgColor'>
                        <td>$serial</td>
                        <td>$oneData->id</td>
                        <td>$oneData->book_name</td>
                        <td>$oneData->author_name</td>
                        <td><a href='view.php?id=$oneData->id'>view</a></td>
                    </tr>
                ";

                $serial++;
            }
        ?>

    </table>

</body>
</html>